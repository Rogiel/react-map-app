import './App.css'
import Coordinate from './Model/Coordinate'
import CoordinateBox from './Model/CoordinateBox'
import Location, { ILocationDataSource } from './Model/Location'
import Map from './Component/Map'
import MarkerForm from './Component/MarkerForm'
import React from 'react'

/**
 * An interface that defines the `App` component properties
 */
interface IAppProps {
  /**
   * The data source to load, insert and remove location markers from.
   */
  dataSource: ILocationDataSource
}

/**
 * An interface that defines the `App` component state
 */
interface IAppState {
  /**
   * The coordinate box currently visible on the map.
   */
  box?: CoordinateBox

  /**
   * A set of locations visible on the map. These locations are filtered by the area in `box`.
   */
  locations: Location[]

  /**
   * The base location template
   */
  formInitialLocation?: Coordinate
}

export default class App extends React.Component<IAppProps, IAppState> {
  constructor (props: IAppProps) {
    super(props)
    this.state = {
      locations: []
    }
    this.reload()
    this.handleRemoveLocation = this.handleRemoveLocation.bind(this)
    this.handleAddLocation = this.handleAddLocation.bind(this)
  }

  /**
   * Reloads the location markers from the data source.
   */
  reload () {
    // Query the available location markers. If `this.state.box` is
    // undefined, query all available locations.
    //
    let promise: Promise<Generator<Location>>
    if (this.state.box === undefined) {
      promise = this.props.dataSource.enumerate()
    } else {
      promise = this.props.dataSource.queryArea(this.state.box)
    }

    // Update internal component state.
    //
    promise.then((enumeration) => {
      const arr = Array.from(enumeration)
      this.setState({
        locations: arr
      })
    })
  }

  /**
   * Removes the given location marker from the data source.
   * @param location  the location to be removed
   */
  async handleRemoveLocation (location: Location) {
    await this.props.dataSource.remove(location)
    this.reload()
  }

  /**
   * Adds a new location into the data source.
   * @param location  the location to be inserted
   */
  handleAddLocation (location: Location) {
    this.props.dataSource
      .insert(location)
      .then(() => this.reload())
  }

  /**
   * Renders the main app view.
   */
  render () {
    return (
      <div className='App'>
        <div className='App-map-container'>
          <Map
            locations={this.state.locations} onRemoveLocation={this.handleRemoveLocation}
            googleAPIKey='AIzaSyD2xZj7QYXpSBpdCbrbfj8OmIQNLjyx5J0'
            initialZoom={0} initialCenter={new Coordinate(0, 0)}
            onChange={(center, box) => {
              this.setState({ box: box })
              this.reload()
            }}
            onClick={(coordinate) => {
              this.setState({
                formInitialLocation: coordinate
              })
            }}
          />
        </div>

        <div className='App-sidebar'>
          <h1>Add new location</h1>
          <MarkerForm initialCoordinate={this.state.formInitialLocation} onSubmit={this.handleAddLocation} />
          <p><b>TIP</b>: you can click on a location on the map to have it's coordinates automatically copied.</p>
        </div>
      </div>
    )
  }
}
