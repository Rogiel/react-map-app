/* eslint-env jest */

import CoordinateBox from './CoordinateBox'
import Coordinate from './Coordinate'

describe('CoordinateBox', () => {
  test('constructor', () => {
    let box = new CoordinateBox(
      new Coordinate(0, 0),
      new Coordinate(1, 1))
    expect(box.minimum.latitude).toBe(0)
    expect(box.minimum.longitude).toBe(0)
    expect(box.maximum.latitude).toBe(1)
    expect(box.maximum.longitude).toBe(1)

    box = new CoordinateBox(
      new Coordinate(5, 7),
      new Coordinate(13, 32))
    expect(box.minimum.latitude).toBe(5)
    expect(box.minimum.longitude).toBe(7)
    expect(box.maximum.latitude).toBe(13)
    expect(box.maximum.longitude).toBe(32)

    // minimum > maximum -- should throw
    expect(() => new CoordinateBox(
      new Coordinate(2, 2),
      new Coordinate(1, 1))).toThrow()
  })

  test('contains', () => {
    var box = new CoordinateBox(
      new Coordinate(10, 20),
      new Coordinate(50, 50))

    expect(box.contains(new Coordinate(10, 20))).toBeTruthy()
    expect(box.contains(new Coordinate(50, 50))).toBeTruthy()

    expect(!box.contains(new Coordinate(0, 0))).toBeTruthy()
    expect(box.contains(new Coordinate(15, 40))).toBeTruthy()
  })
})
