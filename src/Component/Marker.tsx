import './Marker.css'
import Location from '../Model/Location'
import React from 'react'

interface IMarkerProps {
  /**
   * The marker latitude value
   */
  lat: number

  /**
   * The marker longitude value
   */
  lng: number

  /**
   * The marker location object.
   */
  location: Location

  /**
   * An event that gets called whenever the marker remove button gets clicked.
   */
  onRemoveClick: () => void;
}

export default class Marker extends React.Component<IMarkerProps> {
  render () {
    return (
      <div className='Marker'>
        <div className='Marker-circle' onClick={() => this.props.onRemoveClick()}>
                    X
        </div>
        <div className='Marker-name'>
          {this.props.location.name}
        </div>
      </div>
    )
  }
}
