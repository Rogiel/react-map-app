import '../App.css'
import Coordinate from '../Model/Coordinate'
import CoordinateBox from '../Model/CoordinateBox'
import GoogleMapReact from 'google-map-react'
import Location from '../Model/Location'
import Marker from './Marker'
import React from 'react'

/**
 * An interface describing attributes supported by the Map component.
 */
interface IMapProps {
  /**
   * The Google API key for the Google Maps JavaScript API. If not set, will
   * run Google Maps in development mode.
   */
  googleAPIKey?: string

  /**
   * The locations to be displayed on the map. If set, will display a marker for
   * each location given.
   */
  locations?: Location[]

  /**
   * An event triggered whenever the user clicks the remove button on a location marker.
   * This event can be used to remove locations from the underlying storage engine.
   *
   * @param location
   */
  onRemoveLocation?: (location: Location) => void;

  /**
   * An event that gets triggered whenever the maps is moved. This event can be used to
   * properly query `locations` that are visible in the given `box`.
   *
   * @param center    the new map center
   * @param box       the new map box
   */
  onChange?: (center: Coordinate, box: CoordinateBox) => void;

  /**
   * An event that gets triggered whenever a location on the map gets clicked.
   * @param coordinate    the clicking coordinate
   */
  onClick?: (coordinate: Coordinate) => void;

  /**
   * The map's initial center coordinate. If not set, defaults to (0, 0).
   */
  initialCenter?: Coordinate

  /**
   * The map's initial zoom level. If not set, defaults to `0`.
   */
  initialZoom?: number
}

/**
 * The `Map` component displays a Google Maps map that can be browsed and
 * can have optional markers displayed on it.
 *
 * See `IMapProps` for properties available on the `Map`.
 */
export default class Map extends React.Component<IMapProps> {
  render () {
    return (
      <div className='Map' style={{ height: '100%', width: '100%' }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: this.props.googleAPIKey ?? '' }}
          onChange={(e) => {
            if (this.props.onChange == null) return
            if (e.bounds.sw.lng < -180 || e.bounds.sw.lng > 180.0) return // map is loading

            const center = new Coordinate(e.center.lat, e.center.lng)
            const box = new CoordinateBox(
              new Coordinate(e.bounds.sw.lat, e.bounds.sw.lng),
              new Coordinate(e.bounds.ne.lat, e.bounds.ne.lng)
            )
            this.props.onChange(center, box)
          }}
          onClick={(e) => {
            if (this.props.onClick == null) return
            this.props.onClick(new Coordinate(e.lat, e.lng))
          }}
          defaultCenter={{
            lat: this.props.initialCenter?.latitude ?? 0,
            lng: this.props.initialCenter?.longitude ?? 0
          }}
          defaultZoom={this.props.initialZoom ?? 0}
        >
          {this.props.locations?.map(location => <Marker
            key={location.name}
            lat={location.coordinate.latitude} lng={location.coordinate.longitude}
            location={location}
            onRemoveClick={() => {
              if (this.props.onRemoveLocation) this.props.onRemoveLocation(location)
            }}
          /> /* eslint-disable-line */)}
          {this.props.children}
        </GoogleMapReact>
      </div>
    )
  }
}
