import Coordinate from './Coordinate'

export default class CoordinateBox {
  public minimum: Coordinate;
  public maximum: Coordinate;

  constructor (minimum: Coordinate, maximum: Coordinate) {
    if (minimum.latitude > maximum.latitude ||
          minimum.longitude > maximum.longitude) {
      throw new Error('Invalid `minimum` and `maximum` values!')
    }

    this.minimum = minimum
    this.maximum = maximum
  }

  contains (coordinate: Coordinate) {
    if (coordinate.latitude < this.minimum.latitude) { return false }
    if (coordinate.longitude < this.minimum.longitude) { return false }

    if (coordinate.latitude > this.maximum.latitude) { return false }
    if (coordinate.longitude > this.maximum.longitude) { return false }

    return true
  }
}
