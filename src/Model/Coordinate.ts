export default class Coordinate {
  /**
   * The coordinate latitude is a geographic coordinate that specifies the north–south
   * position of a point on the Earth's surface.
   *
   * Ranges from -90 to 90 degrees.
   */
  public latitude: number;

  /**
   * The coordinate longitude is a geographic coordinate that specifies the east–west
   * position of a point on the Earth's surface.
   *
   * Ranges from -180 to 180 degrees.
   */
  public longitude: number;

  /**
   * Create a new `Coordinate.
   * @param latitude  the coordinate latitude
   * @param longitude the coordinate longitude
   */
  constructor (latitude: number, longitude: number) {
    function between (x: number, min: number, max: number) {
      return x >= min && x <= max
    }

    if (!between(latitude, -90, 90)) {
      throw new Error('Invalid latitude value ' + latitude + '! Latitudes must be between -90 and 90.')
    }
    if (!between(longitude, -180, 180)) {
      throw new Error('Invalid longitude value ' + longitude + '! Longitudes must be between -180 and 180.')
    }

    this.latitude = latitude
    this.longitude = longitude
  }
}
