import Coordinate from './Coordinate'
import CoordinateBox from './CoordinateBox'

export default class Location {
  public name: string;
  public coordinate: Coordinate;

  constructor (name: string, coordinate: Coordinate) {
    this.name = name
    this.coordinate = coordinate
  }
}

export interface ILocationDataSource {
  /**
   * Inserts a new location into the data source.
   * @param location  the location to be inserted
   */
  insert(location: Location): Promise<void>;

  /**
   * Removes an existing location from the data source.
   * @param location  the location to be removed
   */
  remove(location: Location): Promise<void>;

  /**
   * Enumerates all locations in the data source.
   */
  enumerate(): Promise<Generator<Location>>;

  /**
   * Queries for all locations within the given box.
   * @param box   the rectangular area to query locations for
   */
  queryArea(box: CoordinateBox): Promise<Generator<Location>>
}

export class MemoryLocationDataSource implements ILocationDataSource {
  private items: Location[];

  constructor (items: Location[]|null = null) {
    this.items = items ?? []
  }

  async insert (location: Location): Promise<void> {
    this.items.push(location)
  }

  async remove (location: Location): Promise<void> {
    this.items = this.items.filter(function (item) {
      return item !== location
    })
  }

  async enumerate (): Promise<Generator<Location>> {
    function * generator (items: Location[]) {
      for (const location of items) {
        yield location
      }
    }
    return generator(this.items)
  }

  async queryArea (box: CoordinateBox): Promise<Generator<Location>> {
    function * generator (items: Location[]) {
      for (const location of items.filter((item) => box.contains(item.coordinate))) {
        yield location
      }
    }
    return generator(this.items)
  }
}
