/* eslint-env jest */

import Coordinate from './Coordinate'

describe('Coordinate', () => {
  test('constructor', () => {
    let coordinate = new Coordinate(10, 20)
    expect(coordinate.latitude).toBe(10)
    expect(coordinate.longitude).toBe(20)

    coordinate = new Coordinate(64, 34)
    expect(coordinate.latitude).toBe(64)
    expect(coordinate.longitude).toBe(34)

    coordinate = new Coordinate(-90, 180)
    expect(coordinate.latitude).toBe(-90)
    expect(coordinate.longitude).toBe(180)

    coordinate = new Coordinate(90, -180)
    expect(coordinate.latitude).toBe(90)
    expect(coordinate.longitude).toBe(-180)

    // should throw if out of bounds
    expect(() => new Coordinate(+90.1, 0)).toThrow()
    expect(() => new Coordinate(-90.1, 0)).toThrow()
    expect(() => new Coordinate(0, +180.1)).toThrow()
    expect(() => new Coordinate(0, -180.1)).toThrow()
  })
})
