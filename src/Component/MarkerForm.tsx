import '../App.css'
import Coordinate from '../Model/Coordinate'
import Location from '../Model/Location'
import React, { ChangeEvent } from 'react'

/**
 * An interface describing attributes supported by the `MarkerForm` component.
 */
interface IMarkerFormProps {
    /**
     * The location to be displayed on the form. If any.
     */
    location?: Location

    /**
     * The initial coordinate to be used in the form. If null, no initial
     * coordinate will be used.
     */
    initialCoordinate?: Coordinate

    /**
     * An event triggered whenever the user submits a form.
     * @param location  the new location data
     */
    onSubmit: (location: Location) => void;
}

/**
 * An interface describing the state of the `MarkerForm` component.
 */
interface IMarkerFormState {
    /**
     * The location name value (as shown in the form)
     */
    name?: string

    /**
     * The location latitude value (as shown in the form)
     */
    latitude?: number

    /**
     * The location longitude value (as shown in the form)
     */
    longitude?: number

    /**
     * The initial coordinate. Used to set coordinates from the map.
     */
    initialCoordinate?: Coordinate
}

export default class MarkerForm extends React.Component<IMarkerFormProps, IMarkerFormState> {
  constructor (props: IMarkerFormProps) {
    super(props)
    this.state = {
      name: props.location?.name,
      latitude: props.location?.coordinate.latitude ?? props.initialCoordinate?.latitude,
      longitude: props.location?.coordinate.longitude ?? props.initialCoordinate?.longitude,
      initialCoordinate: props.initialCoordinate
    }
  }

  /**
     * An callback used to reset the `latitude` and `longitude` coordinates
     * if `initialCoordinate` has changed.
     */
  componentDidUpdate (prevProps: Readonly<IMarkerFormProps>, prevState: Readonly<IMarkerFormState>, snapshot?: any): void {
    if (JSON.stringify(this.state.initialCoordinate) !== JSON.stringify(this.props.initialCoordinate)) {
      this.setState({
        latitude: this.props.initialCoordinate?.latitude,
        longitude: this.props.initialCoordinate?.longitude,
        initialCoordinate: this.props.initialCoordinate
      })
    }
  }

  render () {
    const trackChanges = (e: ChangeEvent) => {
      const newState = {}
      // @ts-ignore
      newState[e.target.name] = e.target.value
      this.setState(newState)
    }

    return (
      <form
        className='MarkerForm'
        onSubmit={(e) => {
          this.props.onSubmit(new Location(
            this.state.name ?? '',
            new Coordinate(
              this.state.latitude ?? 0,
              this.state.longitude ?? 0)))
          // reset form
          this.setState({
            name: undefined,
            latitude: undefined,
            longitude: undefined
          })
          e.preventDefault()
        }}
      >
        <div className='App-sidebar-row'>
          <input
            name='name' type='text' placeholder='Label'
            value={this.state.name ?? ''} pattern='.{1,}'
            onChange={trackChanges}
          />
        </div>
        <div className='App-sidebar-row'>
          <input
            name='latitude' type='number' datatype='numeric' placeholder='Latitude'
            min='-90.0' max='90.0' step='any'
            value={this.state.latitude ?? ''}
            onChange={trackChanges}
          />
        </div>
        <div className='App-sidebar-row'>
          <input
            name='longitude' type='number' datatype='numeric' placeholder='Longitude'
            min='-180.0' max='180.0' step='any'
            value={this.state.longitude ?? ''}
            onChange={trackChanges}
          />
        </div>
        <div className='App-sidebar-row'>
          <button type='submit'>Add Location</button>
        </div>
      </form>
    )
  }
}
