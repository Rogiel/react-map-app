/* eslint-env jest */

import Location, { MemoryLocationDataSource } from './Location'

import Coordinate from './Coordinate'
import CoordinateBox from './CoordinateBox'

describe('MemoryLocationDataSource', () => {
  describe('insert', () => {
    const dataSource = new MemoryLocationDataSource()

    it('should add item', async () => {
      await dataSource.insert(new Location('Location 1', new Coordinate(0, 0)))
      {
        const values = Array.from(await dataSource.enumerate())
        expect(values.length).toBe(1)
        expect(values.filter((loc: Location) => loc.name === 'Location 1').length).toBe(1)
      }

      await dataSource.insert(new Location('Location 2', new Coordinate(1, 0)))
      {
        const values = Array.from(await dataSource.enumerate())
        expect(values.length).toBe(2)
        expect(values.filter((loc: Location) => loc.name === 'Location 1').length).toBe(1)
      }
    })
  })

  describe('remove', () => {
    const dataSource = new MemoryLocationDataSource([
      new Location('Location 1', new Coordinate(0, 0)),
      new Location('Location 2', new Coordinate(5, 10)),
      new Location('Location 3', new Coordinate(-3, -7))
    ])

    test('should remove a single item', async () => {
      let toBeRemoved = Array.from(await dataSource.enumerate()).filter((loc) => loc.name === 'Location 1')[0]
      await dataSource.remove(toBeRemoved)
      {
        const values = Array.from(await dataSource.enumerate())
        expect(values.length).toBe(2)
        expect(values.filter((loc: Location) => loc.name === 'Location 1').length).toBe(0)
      }

      toBeRemoved = Array.from(await dataSource.enumerate()).filter((loc) => loc.name === 'Location 3')[0]
      await dataSource.remove(toBeRemoved)
      {
        const values = Array.from(await dataSource.enumerate())
        expect(values.length).toBe(1)
        expect(values.filter((loc: Location) => loc.name === 'Location 3').length).toBe(0)
      }
    })

    test('location 2 should remain', async () => {
      const values = Array.from(await dataSource.enumerate())
      expect(values.length).toBe(1)
      expect(values.filter((loc: Location) => loc.name === 'Location 2').length).toBe(1)
    })
  })

  describe('enumerate', () => {
    const dataSource = new MemoryLocationDataSource([
      new Location('Location 1', new Coordinate(0, 0)),
      new Location('Location 2', new Coordinate(5, 10)),
      new Location('Location 3', new Coordinate(-3, -7))
    ])

    test('should return all items', async () => {
      const values = Array.from(await dataSource.enumerate())
      expect(values.length).toBe(3)

      expect(values.filter((loc: Location) => loc.name === 'Location 1').length).toBe(1)
      expect(values.filter((loc: Location) => loc.name === 'Location 2').length).toBe(1)
      expect(values.filter((loc: Location) => loc.name === 'Location 3').length).toBe(1)
    })
  })

  describe('queryArea', () => {
    var dataSource = new MemoryLocationDataSource([
      new Location('Location 1', new Coordinate(0, 0)),
      new Location('Location 2', new Coordinate(5, 10)),
      new Location('Location 3', new Coordinate(-3, -7))
    ])

    test('should return only items within area', async () => {
      let results = Array.from(await dataSource.queryArea(
        new CoordinateBox(
          new Coordinate(-1, -1),
          new Coordinate(1, 1))))
      expect(results.length).toBe(1)
      expect(results.filter((loc: Location) => loc.name === 'Location 1').length).toBe(1)

      results = Array.from(await dataSource.queryArea(
        new CoordinateBox(
          new Coordinate(-1, -1),
          new Coordinate(10, 10))))
      expect(results.length).toBe(2)
      expect(results.filter((loc: Location) => loc.name === 'Location 1').length).toBe(1)
      expect(results.filter((loc: Location) => loc.name === 'Location 2').length).toBe(1)

      results = Array.from(await dataSource.queryArea(
        new CoordinateBox(
          new Coordinate(-10, -10),
          new Coordinate(-1, -1))))
      expect(results.length).toBe(1)
      expect(results.filter((loc: Location) => loc.name === 'Location 3').length).toBe(1)
    })

    test('should not fail for empty sets', async () => {
      const results = Array.from(await dataSource.queryArea( // there are no items in this range!
        new CoordinateBox(
          new Coordinate(30, 100),
          new Coordinate(60, 120))))
      expect(results.length).toBe(0)
    })
  })
})
