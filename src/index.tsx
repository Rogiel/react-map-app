import './index.css'
import * as serviceWorker from './serviceWorker'
import App from './App'
import Coordinate from './Model/Coordinate'
import Location, { MemoryLocationDataSource } from './Model/Location'
import React from 'react'
import ReactDOM from 'react-dom'

const dataSource = new MemoryLocationDataSource([
  new Location('Lajeado', new Coordinate(-29.461289, -51.970405)),
  new Location('Porto Alegre', new Coordinate(-30.036577, -51.215891)),
  new Location('São Paulo', new Coordinate(-23.550763, -46.632947))
])

ReactDOM.render(<App dataSource={dataSource} />, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
